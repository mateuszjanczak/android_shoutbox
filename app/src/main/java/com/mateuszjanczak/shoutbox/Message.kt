package com.mateuszjanczak.shoutbox

class Message(
    var content: String?,
    var login: String?,
    var date: String?,
    var id: String?
)