package com.mateuszjanczak.shoutbox

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class EditActivity : AppCompatActivity() {

    lateinit var loginMsgEdit: TextView
    lateinit var dataMsgEdit: TextView
    lateinit var textMsgEdit: TextView
    lateinit var buttonMsgEdit: Button
    lateinit var inputMsgEdit: EditText
    lateinit var trashMsgEdit: ImageButton
    lateinit var auth: String
    lateinit var service: MessageService

    var BaseUrl = "http://tgryl.pl/shoutbox/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        val login = intent.getStringExtra("login");
        val data = intent.getStringExtra("date");
        val content = intent.getStringExtra("content");
        val id = intent.getStringExtra("id");

        loadData();

        if(login != auth) {
            finish();
        }

        val retrofit = Retrofit.Builder().baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create()).build()
        service = retrofit.create(MessageService::class.java)

        loginMsgEdit = findViewById(R.id.loginMsgEdit);
        dataMsgEdit = findViewById(R.id.dataMsgEdit);
        textMsgEdit = findViewById(R.id.textMsgEdit);
        buttonMsgEdit = findViewById(R.id.buttonMsgEdit);
        inputMsgEdit = findViewById(R.id.inputMsgEdit);
        trashMsgEdit = findViewById(R.id.trashMsgEdit);

        loginMsgEdit.setText(login);
        dataMsgEdit.setText(data);
        textMsgEdit.setText(content)
        inputMsgEdit.setText(content);

        buttonMsgEdit.setOnClickListener{
            val newContent = inputMsgEdit.text.toString()
            editPost(id, newContent, login)
        }

        trashMsgEdit.setOnClickListener{
            deletePost(id)
        }
    }

    fun editPost(id: String, content: String, login: String) {
        val message = MessageModel(content, login);
        val call = service.editPost(id, message)
        call.enqueue(object : Callback<MessageModel?> {
            override fun onFailure(call: Call<MessageModel?>, t: Throwable) {
                Toast.makeText(getApplicationContext(), "Fail",Toast.LENGTH_SHORT).show();
            }

            override fun onResponse(call: Call<MessageModel?>, response: Response<MessageModel?>) {
                setResult(RESULT_OK, intent);
                finish();
            }

        })
    }

    fun deletePost(id: String) {
        val call = service.deletePost(id)
        call.enqueue(object : Callback<MessageModel?> {
            override fun onFailure(call: Call<MessageModel?>, t: Throwable) {
                Toast.makeText(getApplicationContext(), "Fail",Toast.LENGTH_SHORT).show();
            }

            override fun onResponse(call: Call<MessageModel?>, response: Response<MessageModel?>) {
                setResult(RESULT_OK, intent);
                finish();
            }

        })
    }

    fun loadData() {
        val sharedPreferences = getSharedPreferences("shared preferences", Context.MODE_PRIVATE)
        auth = sharedPreferences.getString("login", "")!!
    }


}
