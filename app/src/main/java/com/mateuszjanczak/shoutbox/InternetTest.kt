package com.mateuszjanczak.weather

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log

class InternetTest {
    private var connectivityManager: ConnectivityManager? = null
    private val wifiInfo: NetworkInfo? = null
    private val mobileInfo: NetworkInfo? = null
    private var connected = false
    val isOnline: Boolean
        get() {
            try {
                connectivityManager =
                    context!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connectivityManager!!.activeNetworkInfo
                connected =
                    networkInfo != null && networkInfo.isAvailable && networkInfo.isConnected
                return connected
            } catch (e: Exception) {
                println("CheckConnectivity Exception: " + e.message)
                Log.v("connectivity", e.toString())
            }
            return connected
        }

    companion object {
        private val instance = InternetTest()
        private var context: Context? = null
        fun getInstance(ctx: Context): InternetTest {
            context = ctx.applicationContext
            return instance
        }
    }
}