package com.mateuszjanczak.shoutbox

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mateuszjanczak.shoutbox.MessageAdapter.MessageViewHolder
import com.mateuszjanczak.shoutbox.ui.home.HomeFragment
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.*


class MessageAdapter(val mMessage: ArrayList<Message>) :
    RecyclerView.Adapter<MessageViewHolder>() {

    lateinit var view: View
    var BaseUrl = "http://tgryl.pl/shoutbox/"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        view = LayoutInflater.from(parent.context).inflate(R.layout.shoutbox_item, parent, false)
        return MessageViewHolder(view)
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val currentItem = mMessage[position]
        holder.eLogin.text = currentItem.login
        holder.eContent.text = currentItem.content
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val formatter = SimpleDateFormat("dd.MM.yyyy HH:mm")
        val output = formatter.format(parser.parse(currentItem.date))
        holder.eDate.text = output

        val canEdit = currentItem.login == login();
        holder.eEditBtn.visibility = if(canEdit) View.VISIBLE else View.INVISIBLE
        if(canEdit){
            holder.eEditBtn.setOnClickListener {
                val intent = Intent(view.getContext(), EditActivity::class.java)
                intent.putExtra("id", currentItem.id);
                intent.putExtra("login", currentItem.login);
                intent.putExtra("content", currentItem.content);
                intent.putExtra("date", output);
                if((view.context as MainActivity).isInternet()){
                    (view.context as Activity).startActivityForResult(intent, 1)
                }
            }
        }
    }

    fun deleteMessage(id: String){
        val retrofit = Retrofit.Builder().baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create()).build()
        val service = retrofit.create(MessageService::class.java)
        val call = service.deletePost(id);
        call.enqueue(object : Callback<MessageModel> {
            override fun onFailure(call: Call<MessageModel>, t: Throwable) {
                Log.e("1237", "No niestety nie poszlo");
            }

            override fun onResponse(call: Call<MessageModel>, response: Response<MessageModel>) {
                (view.context as MainActivity).reloadMessages();
            }
        })
    }

    fun login(): String {
        return (view.context as MainActivity).login
    }

    fun removeItem(viewHolder: RecyclerView.ViewHolder){
        val item = mMessage.get(viewHolder.adapterPosition);
        if(item.login == login()) {
            deleteMessage(item.id!!)
            mMessage.removeAt(viewHolder.adapterPosition)
            notifyItemRemoved(viewHolder.adapterPosition)
        }
    }

    override fun getItemCount(): Int {
        return mMessage.size
    }

    class MessageViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var eLogin: TextView
        var eContent: TextView
        var eDate: TextView
        var eEditBtn: Button

        init {
            eLogin = itemView.findViewById(R.id.login)
            eContent = itemView.findViewById(R.id.content)
            eDate = itemView.findViewById(R.id.date)
            eEditBtn = itemView.findViewById(R.id.editBtn)
        }
    }

}