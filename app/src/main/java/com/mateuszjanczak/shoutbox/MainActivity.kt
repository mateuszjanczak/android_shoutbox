package com.mateuszjanczak.shoutbox

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mateuszjanczak.shoutbox.ui.home.HomeFragment


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    lateinit var login: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        navView.setNavigationItemSelectedListener(this)

        loadData();
        if(login.isEmpty()){
            login = "Nieznajomy"
            val intent = Intent(this@MainActivity, SettingsActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_settings -> {
                val intent = Intent(this@MainActivity, SettingsActivity::class.java)
                startActivityForResult(intent, 2)
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val allFragments: List<Fragment> = supportFragmentManager.fragments
        val fragment: HomeFragment = allFragments.get(0).childFragmentManager.fragments.get(0) as HomeFragment
        fragment.getMessages()
    }

    fun reloadMessages(){
        val allFragments: List<Fragment> = supportFragmentManager.fragments
        val fragment: HomeFragment = allFragments.get(0).childFragmentManager.fragments.get(0) as HomeFragment
        fragment.getMessages()
    }

    fun loadData() {
        val sharedPreferences = getSharedPreferences("shared preferences", Context.MODE_PRIVATE)
        login = sharedPreferences.getString("login", "")!!
    }

    fun saveOfflineMsgs(){
        val allFragments: List<Fragment> = supportFragmentManager.fragments
        val fragment: HomeFragment = allFragments.get(0).childFragmentManager.fragments.get(0) as HomeFragment

        val gson = Gson()
        val json = gson.toJson(fragment.shoutbox)

        val sharedPreferences = getSharedPreferences("shared preferences", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("offline", json)
        editor.apply()
    }

    fun readOfflineMsgs(){
        val allFragments: List<Fragment> = supportFragmentManager.fragments
        val fragment: HomeFragment = allFragments.get(0).childFragmentManager.fragments.get(0) as HomeFragment
        val sharedPreferences = getSharedPreferences("shared preferences", Context.MODE_PRIVATE)

        val gson = Gson()
        val response: String = sharedPreferences.getString("offline", "")!!
        val lstArrayList: ArrayList<Message> = gson.fromJson(
            response,
            object : TypeToken<List<Message?>?>() {}.type
        )

        fragment.shoutbox.clear();
        fragment.shoutbox.addAll(lstArrayList)
    }

    fun isInternet(): Boolean {
        val allFragments: List<Fragment> = supportFragmentManager.fragments
        val fragment: HomeFragment = allFragments.get(0).childFragmentManager.fragments.get(0) as HomeFragment
        return fragment.isInternet()
    }

}
