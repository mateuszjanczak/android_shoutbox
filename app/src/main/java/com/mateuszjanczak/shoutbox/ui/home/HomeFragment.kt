package com.mateuszjanczak.shoutbox.ui.home

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mateuszjanczak.shoutbox.*
import com.mateuszjanczak.weather.InternetTest
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class HomeFragment : Fragment() {

    private lateinit var service: MessageService
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var mRecycleView: RecyclerView
    private lateinit var mAdapter: RecyclerView.Adapter<*>
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    public lateinit var shoutbox: ArrayList<Message>
    private lateinit var login: String
    private lateinit var swipeContainer: SwipeRefreshLayout

    lateinit var buttonMsgPost: Button
    lateinit var inputMsgPost: EditText
    lateinit var errorMsgPost: TextView
    lateinit var handler: Handler

    
    var BaseUrl = "http://tgryl.pl/shoutbox/"

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)

        shoutbox = ArrayList<Message>()
        mRecycleView = root.findViewById(R.id.recyclerView) as RecyclerView
        mRecycleView.setHasFixedSize(true)
        mLayoutManager = LinearLayoutManager(activity)
        mAdapter = MessageAdapter(shoutbox)
        mRecycleView.setLayoutManager(mLayoutManager)
        mRecycleView.setAdapter(mAdapter)

        val retrofit = Retrofit.Builder().baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create()).build()
        service = retrofit.create(MessageService::class.java)

        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT){
            override fun getMovementFlags(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder
            ): Int {
                loadData();
                if(!isInternet()) return 0
                if(shoutbox.get(viewHolder.adapterPosition).login != login) return 0
                return super.getMovementFlags(recyclerView, viewHolder)
            }

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, position: Int) {
                (mAdapter as MessageAdapter).removeItem(viewHolder)
            }
        }

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(mRecycleView)

        buttonMsgPost = root.findViewById<Button>(R.id.buttonMsgPost)
        inputMsgPost = root.findViewById<EditText>(R.id.inputMsgPost)
        errorMsgPost = root.findViewById<TextView>(R.id.error)

        buttonMsgPost.setOnClickListener {
            val main = activity as MainActivity?
            main?.loadData();
            login = main?.login.toString()
            val message: String = inputMsgPost.text.toString()
            postMessage(message)
            inputMsgPost.setText("")
        }

        getMessages();
        handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                getMessages()
                handler.postDelayed(this, 30000)
            }
        }, 30000)

        swipeContainer = root.findViewById(R.id.swipe) as SwipeRefreshLayout
        swipeContainer.setOnRefreshListener {
            getMessages();
            swipeContainer.setRefreshing(false);
        }

        handler.postDelayed(object : Runnable {
            override fun run() {
                setInternetStatus()
                handler.postDelayed(this, 2000)
            }
        }, 1)

        return root
    }

    private fun setInternetStatus() {
        if (!isInternet()) {
            disableButtons()
            return
        }
        enableButtons()
    }

    fun isInternet(): Boolean {
        val main = activity as MainActivity
        return InternetTest.getInstance(main.applicationContext).isOnline
    }

    fun disableButtons() {
        buttonMsgPost.visibility = View.INVISIBLE
        inputMsgPost.visibility = View.INVISIBLE
        errorMsgPost.visibility = View.VISIBLE
    }

    fun enableButtons() {
        buttonMsgPost.visibility = View.VISIBLE
        inputMsgPost.visibility = View.VISIBLE
        errorMsgPost.visibility = View.INVISIBLE
    }

    fun getMessages(){
        loadData();
        val call = service.getMessages()
        call!!.enqueue(object : Callback<List<MessageModel>> {
            override fun onResponse(
                call: Call<List<MessageModel>>,
                response: Response<List<MessageModel>>
            ) {
                val messageResponse = response.body()
                val lista: List<MessageModel?>? = response.body();
                if (lista != null) {
                    shoutbox.clear()
                    lista.forEach{
                        if (it != null) {
                            shoutbox.add(Message(it.content, it.login, it.date, it.id))
                        }
                    }
                    mAdapter.notifyDataSetChanged()
                    mRecycleView.scrollToPosition(lista.size-1)
                    saveMsgToOffline();
                };
            }

            override fun onFailure(call: Call<List<MessageModel>>, t: Throwable) {
                Log.i("Error", t.message)
                readMsgFromOffline();
            }
        })
    }

    fun saveMsgToOffline(){
        val main = activity as MainActivity?
        main?.saveOfflineMsgs()
    }

    fun readMsgFromOffline(){
        val main = activity as MainActivity?
        main?.readOfflineMsgs()
    }

    fun postMessage(content: String){
        loadData();
        val message = MessageModel(content, login);
        val call = service.createPost(message);
        call.enqueue(object : Callback<MessageModel?> {
            override fun onFailure(call: Call<MessageModel?>, t: Throwable) {
                Log.e("1237", "No niestety nie poszlo");
            }

            override fun onResponse(call: Call<MessageModel?>, response: Response<MessageModel?>) {
                getMessages();
            }
        })
    }

    fun loadData() {
        val main = activity as MainActivity?
        main?.loadData();
        login = main?.login.toString()
    }

    override fun onResume() {
        super.onResume()
        handler.postDelayed(object : Runnable {
            override fun run() {
                getMessages()
                handler.postDelayed(this, 30000)
            }
        }, 30000)
        handler.postDelayed(object : Runnable {
            override fun run() {
                setInternetStatus()
                handler.postDelayed(this, 2000)
            }
        }, 1)
        getMessages();

    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacksAndMessages(null)
    }
}
