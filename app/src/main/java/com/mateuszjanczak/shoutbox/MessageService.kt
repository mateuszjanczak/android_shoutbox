package com.mateuszjanczak.shoutbox

import retrofit2.Call
import retrofit2.http.*


interface MessageService {
    @GET("messages")
    fun getMessages(): Call<List<MessageModel>>

    @POST("message")
    fun createPost(@Body messageItem: MessageModel): Call<MessageModel>

    @PUT("message/{id}")
    fun editPost(@Path("id") id: String, @Body messageItem: MessageModel): Call<MessageModel>

    @DELETE("message/{id}")
    fun deletePost(@Path("id") id: String): Call<MessageModel>
}