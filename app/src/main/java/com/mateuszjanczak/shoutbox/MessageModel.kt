package com.mateuszjanczak.shoutbox

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MessageModel : Serializable {
    @SerializedName("content")
    var content: String? = null

    @SerializedName("login")
    var login: String? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("id")
    var id: String? = null

    constructor(
        content: String?,
        login: String?,
        date: String?,
        id: String?
    ) {
        this.content = content
        this.login = login
        this.date = date
        this.id = id
    }

    constructor() {}
    constructor(content: String?, login: String?) {
        this.content = content
        this.login = login
    }

    constructor(content: String?, login: String?, id: String?) {
        this.content = content
        this.login = login
        this.id = id
    }
}